<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hpv;

class HpvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hpvs = Hpv::latest()->paginate(5);

        return view('hpvs.index',compact('hpvs'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hpvs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'weight' => 'required',
            'height' => 'required',
            'length' => 'required',
            'width' => 'required',
            'trunk_volume' => 'required'
        ]);

        Hpv::create($request->all());

        return redirect()->route('hpvs.index')
                ->with('success','Hpv created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  Hpv  $hpv
     * @return \Illuminate\Http\Response
     */
    public function show(Hpv $hpv)
    {
        return view('hpvs.show',compact('hpv'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Hpv  $hpv
     * @return \Illuminate\Http\Response
     */
    public function edit(Hpv $hpv)
    {
        return view('hpvs.edit',compact('hpv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Hpv  $hpv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hpv $hpv)
    {
        $request->validate([
            'name' => 'required',
            'weight' => 'required',
            'height' => 'required',
            'length' => 'required',
            'width' => 'required',
            'trunk_volume' => 'required'
        ]);

        $hpv->update($request->all());

        return redirect()->route('hpvs.index')
                ->with('success','Hpv updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Hpv  $hpv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hpv $hpv)
    {
        $hpv->delete();

        return redirect()->route('hpvs.index')
            ->with('success','Hpv deleted successfully');

    }
}
