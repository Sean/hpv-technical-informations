<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hpv extends Model
{
    protected $fillable = ['name', 'weight', 'height', 'length', 'width', 'trunk_volume'];
}