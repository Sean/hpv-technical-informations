@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Hpv admin pannel</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('hpvs.create') }}">Create New hpv</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Weight</th>
            <th>Height</th>
            <th>Length</th>
            <th>Width</th>
            <th>Trunk volume</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($hpvs as $hpv)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $hpv->name }}</td>
            <td>{{ $hpv->weight }}</td>
            <td>{{ $hpv->height }}</td>
            <td>{{ $hpv->length }}</td>
            <td>{{ $hpv->width }}</td>
            <td>{{ $hpv->trunk_volume }}</td>
            <td>
                <form action="{{ route('hpvs.destroy',$hpv->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('hpvs.show',$hpv->id) }}">Show</a>
                    <a class="btn btn-primary" href="{{ route('hpvs.edit',$hpv->id) }}">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
    {!! $hpvs->links() !!}
@endsection